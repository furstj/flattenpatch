### Description ###
This application allows us to flatten single patches which are specified in the dictionary.
The tool can flatten all patches that are normal to the x-y x-z and y-z plane. To use this
application we just have to set the normal direction and the point component.
E.g. flatten a patch in y-direction where all points are at the y position 0.03,
we just set the direction to y and the component to 0.03.


### OpenFOAM Versions ###
* Generated for OpenFOAM
* 4.x


### How to Compile and Run it
* Use git for cloning
* Change to the branch you need
* Compile with wmake
* Copy the flattenPatchDict to your system folder
* Modify the dictionary and run the application

### Contributers ###
* Tobias Holzmann M.Eng.
